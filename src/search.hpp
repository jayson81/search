#include <iostream>
#include <string>

using namespace std;
using std::string;

int search(string text, string pattern)
{
	int k = -1;
	bool found = false;
	for(int i = 0; i < text.size(); i++)
	{ 
		if(text[i] == pattern[0])
		{
			found = true;
			for(int j = 1; j < pattern.size(); j++)
			{
				if(text[j+i] != pattern[j])
				{
					found = false;
					break;
				} 
			}
		}
		if(found == true)
		{
			return i;
			break;
		}
	}
	return k;
}
